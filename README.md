# Formality Vim

To install:

```
cd formality-vim
cp ftdetect/formality.vim ~/.vim/ftdetect/.
cp ftplugin/fmcore.vim ~/.vim/ftplugin/.
cp syntax/fmcore.vim ~/.vim/syntax/.
```
