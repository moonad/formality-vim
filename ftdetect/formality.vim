" ftdetect/formality.vim
autocmd BufNewFile,BufRead *.eac setfiletype eacore
autocmd BufNewFile,BufRead *.eatt setfiletype eatt
autocmd BufNewFile,BufRead *.fmc setfiletype fmcore
autocmd BufNewFile,BufRead *.fm setfiletype formality
